# 2 Best Gaming Mouse 2018 Reviews

## 1.Microsoft SideWinder Gaming Mouse
[**The Microsoft SideWinder Gaming Mouse**](https://www.youtube.com/watch?v=UqIthRBN-Zc) is the latest addition to the Microsoft stable of gaming mice and is small and compact without being an uncomfortable fit in the hand.  

![](https://i.imgur.com/wtfiLYn.png)  
The mouse is styled for use for right or left handed people and the buttons can be customized accordingly. This mouse is designed though more for everyday use and the range of adjustability on the dpi gives this mouse an edge in graphics packages, also being capable of adjustment whist in motion. The MS SideWinder comes with included software on board allowing you to make small micro programs that when you are gaming will allow you to control your game character with fast action precision; just one click can make your character evade a rearward attack.  

The mouse comes into its own though with its impressive array of dpi settings ranging from 2000 down to 400 and can be adjusted on the move so precision is ensured. The one major complaint that customers and reviewers of this mouse have found is that the two side buttons are set just slightly too far forward to be of much value to the gamer.  

In essence, then the SideWinder is more valuable as a precision graphics tool than a [**gaming mouse**](https://medium.com/@techwhiff/best-gaming-mouse-1ff3d1e95f23), however if used within a gaming competition environment it is more than capable of holding its own and can be set up for either hand use so left handed people will feel right at home when using the mouse. With just five buttons to program the mouse it is easily customizable to suit the user’s personal preferences and with the capability of adding macro functions to the buttons through the onboard driver software, this mouse is worth considering. With a very reasonable price tag attached and smooth gliding this laser mouse is very highly praised.  

## 2\. Mad Catz Cyborg R.A.T. 9 Gaming Mouse
The traditional mouse is now going out of fashion; enter the RAT… the Mad Catz Cyborg R.A.T. 9 Gaming Mouse is the wireless version of the popular RAT 7 series from the same manufacturer. This mouse, like its previous incarnation, is fully adjustable to fit even the largest of hands and has the same dpi sensitivity at 5600\. What sets this mouse apart though from its wired cousin is the wireless technology that has been included in the package as a whole.  

The Cyborg R.A.T. 9 Mouse has a small screwdriver set in the tail of the mouse that can be used to adjust all manner of hand settings and mouse plates; the integrated palm plate can be removed to allow for weight adjustment of up to 45 grams of additional weight, to be added or removed on a central metal spindle running from the center of the mouse towards the rear.  

This awesome mouse comes supplied with a neat box to hold all the extra parts and a lossless wireless transmission unit that also acts as a battery charger. Two rechargeable batteries are supplied as well which means that you should never run out of battery power as one can be charged whilst the other is in use.  

The Rat is without a doubt one of the most advanced technological gaming mice on the market today and the supplied software makes programming the individual buttons and setting sensitivity a breeze. So if you are looking for a [**really good gaming mouse**](https://www.techwhiff.com/best-gaming-mouse/) that pushes all the right buttons in your control options then you will be hard pushed to find a better mouse. So why not ditch the conventional mouse and opt for a RAT – you will surely not be disappointed.